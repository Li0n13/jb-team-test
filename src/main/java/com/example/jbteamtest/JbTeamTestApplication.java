package com.example.jbteamtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JbTeamTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(JbTeamTestApplication.class, args);
    }

}
